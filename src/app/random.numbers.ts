import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class RandomNumbers {

  constructor(private http: HttpClient) { }

  fetchNumbers(): Observable<Object> {
    return this.http.get("https://atteuncb9j.execute-api.us-east-2.amazonaws.com/default/randomNumbers");
  }
  }
