import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RandomNumbers } from './random.numbers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(private randomNumber: RandomNumbers) {}

  fetchNumbers() {
    return this.randomNumber.fetchNumbers();
  }
}
